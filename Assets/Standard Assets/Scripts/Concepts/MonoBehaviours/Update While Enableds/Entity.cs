using Extensions;
using UnityEngine;
using Pathfinding;
using System.Collections.Generic;

namespace DandaraVR
{
	[ExecuteInEditMode]
	public class Entity : UpdateWhileEnabled, IDestructable
	{
		float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int maxHp;
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		[HideInInspector]
		public bool dead;
		public Transform trs;
		public Rigidbody rigid;
		public Seeker seeker;
		public float moveSpeed;
		public Transform colliderBottom;
		public float targetDistanceFromPathPoints;
		public Animator animator;
		public AnimationEntry[] animationEntries = new AnimationEntry[0];
		public Dictionary<string, AnimationEntry> animationEntriesDict = new Dictionary<string, AnimationEntry>();
		public delegate void OnDeath ();
		public event OnDeath onDeath;
		public AudioClip[] deathAudioClips = new AudioClip[0];
		protected Vector3 move;
		protected Vector3 currentPathPoint;
		List<Vector3> pathPoints = new List<Vector3>();
		float targetDistanceFromPathPointsSqr;

		public virtual void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody>();
				if (seeker == null)
					seeker = GetComponent<Seeker>();
				return;
			}
#endif
			hp = maxHp;
			targetDistanceFromPathPointsSqr = targetDistanceFromPathPoints * targetDistanceFromPathPoints;
			for (int i = 0; i < animationEntries.Length; i ++)
			{
				AnimationEntry animationEntry = animationEntries[i];
				animationEntriesDict.Add(animationEntry.animatorStateName, animationEntry);
			}
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			HandlePathfinding ();
			HandleRotating ();
			HandleMoving ();
		}

		public virtual void HandlePathfinding ()
		{
			if (pathPoints.Count > 1 && (colliderBottom.position - currentPathPoint).sqrMagnitude <= targetDistanceFromPathPointsSqr)
			{
				pathPoints.RemoveAt(0);
				currentPathPoint = pathPoints[0];
			}
		}

		public void SetDestination (Vector3 position)
		{
			seeker.CancelCurrentPathRequest();
			seeker.StartPath(trs.position, position, OnPathCalculated);
		}

		void OnPathCalculated (Path path)
		{
			pathPoints = path.vectorPath;
			if (pathPoints.Count > 0)
				currentPathPoint = pathPoints[0];
		}

		public virtual void HandleRotating ()
		{
			if (pathPoints.Count > 0)
				trs.rotation = Quaternion.LookRotation(currentPathPoint - trs.position, trs.up);
		}
		
		public virtual void HandleMoving ()
		{
			if (pathPoints.Count > 0)
			{
				move = (currentPathPoint - colliderBottom.position).normalized;
				rigid.velocity = move * moveSpeed;
			}
		}

		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public void Death ()
		{
			Destroy(gameObject);
			// PlayAnimationEntry ("Death");
			// AudioManager.instance.MakeSoundEffect (deathAudioClips[Random.Range(0, deathAudioClips.Length)], trs.position);
			if (onDeath != null)
			{
				onDeath ();
				onDeath = null;
			}
		}

		public override void OnDestroy ()
		{
			base.OnDestroy ();
			onDeath = null;
		}

		public void PlayAnimationEntry (string name)
		{
			animationEntriesDict[name].Play ();
		}

	}
}