using UnityEngine;

namespace DandaraVR
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(LineRenderer))]
	public class Laser : UpdateWhileEnabled
	{
		public LineRenderer line;
		public LayerMask whatBlocksMe;
		public Transform trs;
		public RaycastHit hit;
		public float maxLength;
		Vector3[] points = new Vector3[2];

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (line == null)
					line = GetComponent<LineRenderer>();
				if (trs == null)
					trs = GetComponent<Transform>();
				return;
			}
#endif
			base.OnEnable ();
		}

		public override void DoUpdate ()
		{
			points[0] = trs.position;
			if (Physics.Raycast(trs.position, trs.forward, out hit, maxLength, whatBlocksMe))
				points[1] = hit.point;
			else
				points[1] = trs.position + trs.forward * maxLength;
			line.SetPositions(points);
		}
	}
}
