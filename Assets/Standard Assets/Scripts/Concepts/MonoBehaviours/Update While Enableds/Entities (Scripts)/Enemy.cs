using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace DandaraVR
{
	public class Enemy : Entity, ISpawnable
	{
		public int prefabIndex;
		public int PrefabIndex
		{
			get
			{
				return prefabIndex;
			}
		}
		public Collider[] colliders = new Collider[0];
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public BulletPatternEntryDontCollideWithShooter[] bulletPatternEntriesDontCollideWithShooter = new BulletPatternEntryDontCollideWithShooter[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public float targetDistanceFromPlayer;
		public Transform eyesTrs;
		public float visionDegrees;
		public float visionSphereCastRadius;
		public LayerMask whatBlocksVision;
		public float minPlayerPositionChangeToRepath;
		public Patrol patrol;
		public float destinationDistanceWhileEscaping;
		protected Vector3 lastKnownPlayerPosition;
		protected float targetDistanceFromPlayerSqr;
		protected bool canSeePlayer;
		float minPlayerPositionChangeToRepathSqr;

		public override void Awake ()
		{
			base.Awake ();
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (eyesTrs == null)
					eyesTrs = trs.Find("Eyes");
				return;
			}
#endif
			for (int i = 0; i < colliders.Length; i ++)
			{
				Collider collider = colliders[i];
				for (int i2 = i + 1; i2 < colliders.Length; i2 ++)
				{
					Collider collider2 = colliders[i2];
					Physics.IgnoreCollision(collider, collider2, true);
				}
			}
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
			for (int i = 0; i < bulletPatternEntriesDontCollideWithShooter.Length; i ++)
			{
				BulletPatternEntryDontCollideWithShooter bulletPatternEntryDontCollideWithShooter = bulletPatternEntriesDontCollideWithShooter[i];
				bulletPatternEntriesDict.Add(bulletPatternEntryDontCollideWithShooter.name, bulletPatternEntryDontCollideWithShooter);
			}
			targetDistanceFromPlayerSqr = targetDistanceFromPlayer* targetDistanceFromPlayer;
			minPlayerPositionChangeToRepathSqr = minPlayerPositionChangeToRepath * minPlayerPositionChangeToRepath;
		}

		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
		
		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			canSeePlayer = CanSeePlayer();
			HandleAttacking ();
			base.DoUpdate ();
		}

		public virtual void HandleAttacking ()
		{
		}

		public override void HandlePathfinding ()
		{
			if (canSeePlayer)
			{
				Vector3 playerPosition = Player.instance.trs.position;
				if ((lastKnownPlayerPosition - playerPosition).sqrMagnitude >= minPlayerPositionChangeToRepathSqr)
				{
					if ((trs.position - playerPosition).sqrMagnitude < targetDistanceFromPlayerSqr)
					{
						Vector3 destination = trs.position;
						destination += (trs.position - playerPosition) * destinationDistanceWhileEscaping;
						SetDestination (destination);
					}
					else
						SetDestination (playerPosition);
					lastKnownPlayerPosition = playerPosition;
				}
			}
			base.HandlePathfinding ();
		}
		
		public override void HandleRotating ()
		{
			if (canSeePlayer)
				trs.rotation = Quaternion.LookRotation(Player.instance.trs.position - trs.position, trs.up);
			else
				base.HandleRotating ();
		}

		void OnTriggerEnter (Collider other)
		{
			OnTriggerStay (other);
		}

		void OnTriggerStay (Collider other)
		{
			Player player = other.GetComponent<Player>();
			if (player != null)
			{
				Transform playerTrs = other.GetComponent<Transform>();
				Vector3 eyesToPlayer = playerTrs.position - eyesTrs.position;
				if (Vector3.Angle(eyesTrs.forward, eyesToPlayer) <= visionDegrees / 2)
				{
					RaycastHit hit;
					if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, eyesToPlayer, out hit, eyesToPlayer.magnitude, whatBlocksVision, QueryTriggerInteraction.Ignore) && hit.collider == other)
						WakeUp ();
				}
			}
		}

		public void WakeUp ()
		{
			patrol.enabled = false;
			enabled = true;
		}

		bool CanSeePlayer ()
		{
			Vector3 eyesToPlayer = Player.instance.trs.position - eyesTrs.position;
			if (Vector3.Angle(eyesTrs.forward, eyesToPlayer) <= visionDegrees / 2)
			{
				RaycastHit hit;
				if (Physics.SphereCast(eyesTrs.position, visionSphereCastRadius, eyesToPlayer, out hit, eyesToPlayer.magnitude, whatBlocksVision, QueryTriggerInteraction.Ignore))
				{
					Player player = hit.collider.GetComponent<Player>();
					if (player != null)
						return true;
				}
			}
			return false;
		}
	}
}