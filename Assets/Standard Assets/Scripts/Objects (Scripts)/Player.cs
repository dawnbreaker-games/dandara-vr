using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace DandaraVR
{
	public class Player : SingletonUpdateWhileEnabled<Player>, IDestructable
	{
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public int MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		public int maxHp;
		public Hand leftHand;
		public Hand rightHand;
		public Transform trs;
		public Rigidbody rigid;
		public CapsuleCollider capsuleCollider;
		public Transform colliderBottom;
		public float flySpeed;
		public float maxFlyDistance;
		public LayerMask whatICantFlyToward;
		public LayerMask whatIFlyToward;
		public BulletPatternEntry[] bulletPatternEntries = new BulletPatternEntry[0];
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public Animator animator;
		public float minAngleBetweenCollisionNormalsToKill;
		public float flyingFractionToFullTurn;
		bool isFlying;
		Collider grapplingCollider;
		bool dead;
		float hp;

		public override void Awake ()
		{
			base.Awake ();
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
		}

		public override void OnEnable ()
		{
			base.OnEnable ();
			hp = maxHp;
		}

		public override void DoUpdate ()
		{
			HandleAiming ();
			HandleMovement ();
			HandleRotation ();
		}

		void OnValidate ()
		{
#if UNITY_EDITOR
			leftHand.laser.maxLength = maxFlyDistance;
			rightHand.laser.maxLength = maxFlyDistance;
			leftHand.laser.whatBlocksMe = whatIFlyToward.AddToMask(whatICantFlyToward);
			rightHand.laser.whatBlocksMe = whatIFlyToward.AddToMask(whatICantFlyToward);
#endif
		}

		void HandleAiming ()
		{
			HandleAiming (leftHand);
			HandleAiming (rightHand);
		}

		void HandleAiming (Hand hand)
		{
			RaycastHit hit;
			if (!Physics.SphereCast(trs.position, capsuleCollider.height / 2, hand.trs.forward, out hit, maxFlyDistance, whatICantFlyToward) && Physics.Raycast(trs.position, hand.trs.forward, out hit, maxFlyDistance, whatIFlyToward) && hit.collider != grapplingCollider)
			{
				if (hit.point != hand.hit.point)
				{
					hand.ghostTrs.position = hit.point;
					hand.ghostTrs.rotation = Quaternion.LookRotation(hit.point - hand.hit.point, hit.normal);
					hand.hit = hit;
				}
				hand.ghostTrs.gameObject.SetActive(true);
			}
			else
				hand.ghostTrs.gameObject.SetActive(false);
		}

		void HandleMovement ()
		{
			if (isFlying)
				return;
			if (VRCameraRig.instance.leftHand.triggerInput && !VRCameraRig.instance.leftHand.previousTriggerInput)
				FlyToward (leftHand);
			else if (VRCameraRig.instance.rightHand.triggerInput && !VRCameraRig.instance.rightHand.previousTriggerInput)
				FlyToward (rightHand);
		}

		void FlyToward (Hand hand)
		{
			if (!hand.ghostTrs.gameObject.activeSelf)
				return;
			isFlying = true;
			grapplingCollider = hand.hit.collider;
			StartCoroutine(FlyRoutine (hand));
		}

		IEnumerator FlyRoutine (Hand hand)
		{
			float distanceFlown = 0;
			Quaternion startRotation = trs.rotation;
			Quaternion endRotation = hand.ghostTrs.rotation;
			Vector3 hitPoint = hand.hit.point;
			Vector3 flyVector = hand.trs.forward * flySpeed;
			while (distanceFlown < hand.hit.distance)
			{
				rigid.velocity = flyVector;
				distanceFlown += flySpeed * Time.deltaTime;
				float slerpAmount = distanceFlown / hand.hit.distance / flyingFractionToFullTurn;
				if (slerpAmount < 1)
					trs.rotation = Quaternion.Slerp(startRotation, endRotation, slerpAmount);
				else
					trs.rotation = endRotation;
				yield return new WaitForEndOfFrame();
			}
			rigid.velocity = Vector3.zero;
			trs.position += hitPoint - colliderBottom.position;
			isFlying = false;
		}

		void HandleRotation ()
		{
			if (isFlying)
				return;
			trs.rotation = Quaternion.LookRotation(VRCameraRig.instance.eyesTrs.forward, trs.up);
		}
		
		public void ShootBulletPatternEntry (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}

		public void TakeDamage (float amount)
		{
			if (dead)
				return;
			hp = Mathf.Clamp(hp - amount, 0, MaxHp);
			if (hp == 0)
			{
				dead = true;
				Death ();
			}
		}

		public void Death ()
		{
			_SceneManager.instance.RestartSceneWithoutTransition ();
		}

		void OnCollisionStay (Collision coll)
		{
			ContactPoint[] contactPoints = new ContactPoint[0];
			coll.GetContacts(contactPoints);
			for (int i = 0; i < contactPoints.Length; i ++)
			{
				ContactPoint contactPoint = contactPoints[i];
				for (int i2 = i + 1; i2 < contactPoints.Length; i2 ++)
				{
					ContactPoint contactPoint2 = contactPoints[i2];
					if (Vector3.Angle(contactPoint.normal, contactPoint2.normal) >= minAngleBetweenCollisionNormalsToKill)
					{
						Death ();
						return;
					}
				}
			}
		}

		[Serializable]
		public class Hand : VRCameraRig.Hand
		{
			public Laser laser;
			public Transform ghostTrs;
			public RaycastHit hit;
		}
	}
}